# Alphabet Recognizer
#### Simple Optical Character Recognizer 

[This Image processing program](http://minhaskamal.github.io/AlphabetRecognizer) can convert image of English writing to text. It simply uses **Template Matching** strategy for character recognition.

### How to Run?
1. Import the project in your IDE, & integrate **[Egami](https://github.com/MinhasKamal/Egami)** in the build path.
2. For training the machine run **[Train.java](https://github.com/MinhasKamal/AlphabetRecognizer/blob/master/src/com/minhaskamal/alphabetRecognizer/Train.java)**.
3. For testing run **[Predict.java](https://github.com/MinhasKamal/AlphabetRecognizer/blob/master/src/com/minhaskamal/alphabetRecognizer/Predict.java)** (change file-paths in the *main* method according to your need).

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>Alphabet Recognizer is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
